﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour {

	public float speed = 1f;
    public GameObject player;
    public float offset;

	// Use this for initialization
	void Start () {
        player.GetComponent<PlayerMove>();
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = new Vector2((player.transform.position.x + offset) * -speed, transform.position.y);
	}
}
