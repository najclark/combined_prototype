﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodScript : MonoBehaviour {

    float foodHealth = 40f;
    float currentHealth;
    public bool isHit = false;

    // Use this for initialization
    void Start () {
        currentHealth = foodHealth;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Damage(float damage)
    {
        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            Death();
        }
        isHit = false;
    }

    void Death()
    {
        Destroy(gameObject);
    }
}
